// ============================================================================
// Copyright 2020 Jens Grassel
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from this
// software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ============================================================================
extern crate clap;
extern crate csv;
extern crate rand;
extern crate serde;

use clap::{Arg, App};
use rand::prelude::*;
use std::error::Error;
use std::process;

fn run(source: &[u8]) -> Result<String, Box<dyn Error>> {
    let mut rdr = csv::ReaderBuilder::new()
        .delimiter(b';')
        .has_headers(false)
        .from_reader(source);
    let mut entries: Vec<String> = Vec::new();
    for result in rdr.deserialize() {
        let record: Vec<String> = result?;
        entries.push(record.join(" "))
    }
    let count = entries.len();
    let mut rng = rand::thread_rng();
    let e: usize = rng.gen_range(0, count);
    let candidate = &entries[e];
    Ok(candidate.to_string())
}

fn main() {
    let forenames_german = include_bytes!("forenames/german.txt");
    let surnames_german = include_bytes!("surnames/german.txt");
    let zipcodes_german = include_bytes!("zips/german.csv");

    let params = App::new("Random Identity Generator")
        .version("1.0.0")
        .about("Generate a random identity.")
        .arg(Arg::with_name("country")
            .help("Pick sources for a specific country.")
        )
        .get_matches();

    // Get the country as base for suggestions.
    let (src_forenames, src_surnames, src_zipcodes) = match params.value_of("country").unwrap_or("all").to_lowercase().as_str() {
        "de" | "ger" | "germany" => (forenames_german, surnames_german, zipcodes_german),
        _                        => (forenames_german, surnames_german, zipcodes_german),
    };

    let name = (run(src_forenames), run(src_surnames), run(src_zipcodes));

    match name {
        (Ok(forename), Ok(surname), Ok(address)) => {
            let mut name = forename;
            name.push_str(&" ");
            name.push_str(&surname);
            name.push_str(", ");
            name.push_str(&address);
            println!("{}", name);
        },
        _ => {
            println!("An error occured!");
            process::exit(1);
        },
    }
}
