# Generate a random identity.

This is a small tool which will generate a random identity.

Please see [CONTRIBUTING.md](CONTRIBUTING.md) for details how to to 
contribute to the project.

## License

This program is distributed under 3-Clause BSD license. See the file 
[LICENSE](LICENSE) for details.

## Installation

Either create a binary with `cargo build --release` and copy it to a place
of your liking or use the `cargo install --path .` command which will 
install it into `$HOME/.cargo/bin`.

